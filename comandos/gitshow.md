# Git Show

Definicao:

O comando git show é usado para exibir informações detalhadas sobre um único commit específico. Ele mostra as alterações introduzidas por aquele commit, bem como metadados associados a ele, como o autor, a data do commit e a mensagem do commit.

Exemplo:

Author: Weiller Carvalho <engweiller@hotmail.com>

Date:   Tue Sep 19 23:42:18 2023 -0300

    docs: Alterando nome completo

diff --git a/README.md b/README.md

index 5dd7d72..f374726 100644

--- a/README.md

+++ b/README.md

@@ -1,6 +1,6 @@

Informações Pessoais
 
-Nome : Weiller
+Nome : Weiller Sousa Amaral Carvalho Mello
 
 Matricula : 192271