# Git Log

Definicao:

O comando git log é usado para listar todos os commits em um repositório Git. Ele exibe uma lista dos commits em ordem cronológica inversa, mostrando informações resumidas sobre cada commit, como o hash, o autor, a data e a mensagem do commit.

Exemplo:

Author: Weiller Carvalho <engweiller@hotmail.com>

Date:   Tue Sep 19 23:42:18 2023 -0300

    docs: Alterando nome completo

commit 89282cf9749165dfb8192014c6936ed5c0d0ad35

Author: Weiller Carvalho <engweiller@hotmail.com>

Date:   Tue Sep 19 23:41:34 2023 -0300

    docs: Adicionando experiencias

commit 750e6c6b734b486451d8d044b3133f7fcca5b8dd

Author: Weiller Carvalho <engweiller@hotmail.com>

Date:   Tue Sep 19 23:39:36 2023 -0300

    docs: Inicializando as infos pessoais