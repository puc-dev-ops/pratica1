# Informações Pessoais

Nome : Weiller Sousa Amaral Carvalho Mello

Matricula : 192271

E-mail : engweiller@hotmail.com

# Experiência

Sou desenvolvedor fullstack e minha linguagem principal é Javascript/Typescript especializado em Backend com NodeJS (Express) e no Frontend com React.js, experiência em bancos de dados relacionais(PostgreSQL), não relacionais(MongoDB), banco de cache(Redis), mapeamento objeto-relacional, testes de integração, testes unitários, testes de volume/carga além de um bom conhecimento em CSS, SASS, HTML, AWS, Docker, Elixir, Java, Selenium. Além disso sou focado em masterizar minhas soft-skills como ser proativo, resiliente em diferentes cenários e bons conhecimentos em metodologias ágeis como SCRUM/Kanban.

Atualmente sou Software Developer Engineer in Test (SDET) na Bureau Works, onde atuo em 2 fronts diferentes, parcialmente como QA e em parte atuando com o time de implementação/dev. Estou fazendo 3 pós graduacoes, uma em Cloud Computing, uma em DevOps e uma em Qualidade e Teste de Softwares. Além disso, sou tecnólogo em desenvolvimento web e engenheiro civil.